import React, {useMemo} from 'react';

export default function List({data, onClick}){
console.log('dataList', data);
const data1 = useMemo(()=>data, [data]);
return data1.map(item=><div onClick={()=>onClick(data)}>{item}</div>);
};
import React, {useState } from 'react';
import Counter from "./counter";
import './App.css';

export const countContext = React.createContext({count:{count1: 0, count2: 0}, setCount: ()=>{}});
function App() {
  const [count, setCount] = useState({count1: 0, count2: 0});
  return (
    <div className="App">
      <countContext.Provider value={{count, setCount}}>
        <Counter style={{width: "300px", height: "200px", marginBottom: "20px"}} type="count1"/>
        <Counter style={{width: "300px", height: "200px", marginBottom: "20px"}} type="count2" />
      </countContext.Provider>
    </div>
  );
}

export default App;

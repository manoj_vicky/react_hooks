import React, {useContext} from 'react';
import {countContext} from "./App";
function Counter(props) {
  const count = useContext(countContext);
  console.log('count', count, props);
  return (
    <div className="App" style={props.style} onClick={()=>count.setCount((prevState)=>{return{
        ...prevState,
        [props.type]: count.count[props.type]+1
    }})}>
        {count.count[props.type]}
    </div>
  );
}

export default React.memo(Counter);
